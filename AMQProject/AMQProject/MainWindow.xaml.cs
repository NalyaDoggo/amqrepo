﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AMQProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AmqDBEntities1 AmqEntities = new AmqDBEntities1();
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var query = (from p in AmqEntities.AmqSongs
                         select p).ToList();
            Search_DataGrid.ItemsSource = query;
        }

        private void Add_AddBtn_Click(object sender, RoutedEventArgs e)
        {
            string SongTrim = Add_SongInfoTB.Text.Trim();
            //split the text at "\n" but since it won't accept \n, this is the work around.
            string[] SongInfo = SongTrim.Split(
                                new[] { Environment.NewLine },
                                StringSplitOptions.None
                            );
            string Series = SongInfo[0];
            //This is a lot of repetitive code. TODO if you have time
            string Title = FindTitle(SongInfo);
            string Artist = FindArtist(SongInfo);
            string Type = FindType(SongInfo);
            string CorrectPeople = Add_PeopleCountTB.Text;
            string Notes = Add_NotesTB.Text;

            AmqSong song = new AmqSong
            {
                Series = Series,
                SongTitle = Title,
                Artist = Artist,
                Type = Type,
                Correct = Add_CorrectCB.IsChecked,
                CorrectPeople = CorrectPeople,
                Notes = Notes
            };
            try
            {
                AmqEntities.AmqSongs.Add(song);
                AmqEntities.SaveChanges();
                Add_OutputTB.Text = Title + "\nSong added!";
                Add_OutputTB.ToolTip = Series + "\n" + Title + "\n" + Artist;
                ClearAddFields();
                Search_DataGrid.ItemsSource = null;
                var query = (from p in AmqEntities.AmqSongs
                             select p).ToList();
                Search_DataGrid.ItemsSource = query;
            }
            catch
            {
                MessageBox.Show("Error in string format. Please refer to \"How to\"");
            }
        }

        private void Add_ClearBtn_Click(object sender, RoutedEventArgs e)
        {
            ClearAddFields();
        }

        public void ClearAddFields()
        {
            Add_SongInfoTB.Text = "";
            Add_PeopleCountTB.Text = "";
            Add_CorrectCB.IsChecked = false;
            Add_NotesTB.Text = "";
        }

        public string FindTitle(string[] SongInfo)
        {
            for (int i = 0; i < SongInfo.Length; i++)
            {
                if (SongInfo[i].Equals("Artist"))
                {
                    return SongInfo[i - 1];
                }
            }
            return null;
        }
        public string FindArtist(string[] SongInfo)
        {
            for (int i = 0; i < SongInfo.Length; i++)
            {
                if (SongInfo[i].Equals("Type"))
                {
                    return SongInfo[i - 1];
                }
            }
            return null;
        }
        public string FindType(string[] SongInfo)
        {
            for (int i = 0; i < SongInfo.Length; i++)
            {
                if (SongInfo[i].Equals("Type"))
                {
                    return SongInfo[i + 2];
                }
            }
            return null;
        }

        private void ExecuteSearch(object sender, TextChangedEventArgs e)
        {
            Task.Delay(500).ContinueWith(_ =>
            {
                Dispatcher.Invoke(() =>
                {
                    Search_DataGrid.ItemsSource = null;
                    var query = SearchQuery();
                    Search_DataGrid.ItemsSource = query;
                });
            });
        }

        private List<AmqSong> SearchQuery()
        {
            string SearchSeries = Search_SeriesBox.Text;
            string SearchTitle = Search_NameBox.Text;
            string SearchArtist = Search_ArtistBox.Text;
            string SearchNotes = Search_NotesBox.Text;
            List<AmqSong> searches;
            searches = (from p in AmqEntities.AmqSongs
                        select p).ToList();
            searches = (from p in searches
                        where p.Artist.IndexOf(SearchArtist, StringComparison.OrdinalIgnoreCase) != -1
                        select p).ToList();
            searches = (from p in searches
                        where p.SongTitle.IndexOf(SearchTitle, StringComparison.OrdinalIgnoreCase) != -1
                        select p).ToList();
            searches = (from p in searches
                        where p.Series.IndexOf(SearchSeries, StringComparison.OrdinalIgnoreCase) != -1
                        select p).ToList();
            if (!String.IsNullOrEmpty(SearchNotes))
            {
                searches = (from p in searches
                            where p.Notes != null
                            select p).ToList();
                searches = (from p in searches 
                            where p.Notes.IndexOf(SearchNotes, StringComparison.OrdinalIgnoreCase) != -1
                            select p).ToList();
            }
            searches = TypeDeterimation(searches);
            return searches;
        }

        private List<AmqSong> TypeDeterimation(List<AmqSong> searches)
        {
            if ((bool)Search_RadioOp.IsChecked)
            {
                searches = (from p in searches
                            where p.Type.Contains("Open")
                            select p).ToList();
                return searches;
            }
            else if ((bool)Search_RadioEd.IsChecked)
            {
                searches = (from p in searches
                            where p.Type.Contains("End")
                            select p).ToList();
                return searches;
            }
            else if ((bool)Search_RadioIn.IsChecked)
            {
                searches = (from p in searches
                            where p.Type.Contains("Insert")
                            select p).ToList();
                return searches;
            }
            else
            {
                return searches;
            }
        }

        private void Search_Radio_Checked(object sender, RoutedEventArgs e)
        {
            Task.Delay(500).ContinueWith(_ =>
            {
                Dispatcher.Invoke(() =>
                {
                    Search_DataGrid.ItemsSource = null;
                    var query = SearchQuery();
                    Search_DataGrid.ItemsSource = query;
                });
            });
        }
    }
}
